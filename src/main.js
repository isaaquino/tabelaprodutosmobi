import Vue from 'vue'
import App from './App.vue'

import _ from 'lodash'

import VueResource from 'vue-resource';

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(VueResource)

import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowLeft, faSearch, faUserSecret, faAngleRight, faAngleLeft, faTrashAlt, faCalendarAlt, faFilter } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import $tabela from './mixins/tabela.js'

library.add(faUserSecret, faArrowLeft, faSearch, faAngleRight, faAngleLeft, faTrashAlt, faCalendarAlt, faFilter)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.mixin($tabela);


new Vue({
  el: '#app',
  render: h => h(App)
})

// define a mixin object

var accents = require('remove-accents');

export default {
	methods: {

		filtra(){

			if(this.tabela.bouse == false){
				this.tabela.bouse = true
				_.delay(() => {

					this.tabela.pagination.currentPage = 1
					this.filtraCore()

					if(this.tabela.bouse == true){
						this.tabela.bouse = false
					}
				}, 500, 'later');


			}
		},

		filtraCore(){
			var filtrado = []

			this.tabela.lista = this.tabela.lista.map((item, index) => {
				item._index = index
				item._exibir = false
				return item
			})

			if(this._isMobile){
				this.tabela.pagination.perPage = 20
			}

			if(this.tabela.busca.trim() != ""){
				filtrado = _.filter(this.tabela.lista, (item) => {

					return this.tabela.columns.some((column) => {
						var it =  accents.remove(item[column] + '').toLowerCase()
						let tb = accents.remove(this.tabela.busca.toLowerCase())
						return it.indexOf(tb) > -1
					});
				});

				filtrado = _.orderBy(filtrado, [this.tabela.column], [this.tabela.inverse]);
			} else {

				// apenas filta a lista
				filtrado = _.orderBy(this.tabela.lista, [this.tabela.column], [this.tabela.inverse]);
			}
			this.setDataLista(filtrado)
		},

		// seta a tabela a ser visualizada
		setDataLista(lista){

			var listAxu = []
			this.tabela.listaChunk = []

			for(var i in lista){
				listAxu.push({_index : Number(lista[i]._index)})
			}

			this.tabela.listaChunk = this.quebrar(lista, this.tabela.pagination.perPage)
			// está fazendo o for na lista sem está filtrada, por isso não tá exibindo
			// tem que fazer uma matris e manipular a lista

			this.tabela.pagination.totalItens = this.tabela.lista.length;
			this.tabela.pagination.totalPages = Math.ceil(this.tabela.pagination.totalItens / this.tabela.pagination.perPage)

			var temp = [];
			for (var i = 1; i <= this.tabela.listaChunk.length; i++) {
				temp.push(i);
			}
			this.tabela.pagination.pageNumbers = temp
		},

		// exibe os itens filtrados da lista
		exibeLista(posicao){

			var len = this.tabela.lista.length;
			for(var i = 0; i < len; i++){
				this.tabela.lista[i]._exibir = false
			}

			if(!_.isNil(this.tabela.listaChunk[posicao])){
				for(var i = 0; i < this.tabela.listaChunk[posicao].length; i++){
					for(var j = 0; j < len; j++){
						if(this.tabela.listaChunk[posicao][i]._index == this.tabela.lista[j]._index){
							this.tabela.lista[j]._exibir = true
							break
						}
					}
				}
			}
		},

		ordernar(coluna){

			this.tabela.column = coluna;

			if(this.tabela.inverse == 'asc'){
				this.tabela.inverse = 'desc'
			} else{
				this.tabela.inverse = 'asc'
			}

			this.tabela.pagination.currentPage = 1

			this.filtra();
		},

		// quando clica no número da páginação
		page (page) {

			this.tabela.pagination.currentPage = page;
			this.exibeLista(page - 1)
		},

		// quebra a lista de item através do número de itens por página
		quebrar (arr, chunkSize) {
			var groups = [], i;
			for (var i = 0; i < arr.length; i += chunkSize) {
				groups.push(arr.slice(i, i + chunkSize));
			}
			return groups;
		},

		// quando clica na seta de próximo
		next () {

			if (this.tabela.pagination.currentPage == this.tabela.pagination.totalPages) {
				return false;
			}

			this.tabela.pagination.currentPage = this.tabela.pagination.currentPage + 1

			this.exibeLista(this.tabela.pagination.currentPage - 1)
		},

		// quando clica na seta de voltar
		previous () {


			if (this.tabela.pagination.currentPage == 1) {
				return false
			}

			this.tabela.pagination.currentPage = this.tabela.pagination.currentPage - 1
			this.exibeLista(this.tabela.pagination.currentPage - 1)
		},
	}
}
